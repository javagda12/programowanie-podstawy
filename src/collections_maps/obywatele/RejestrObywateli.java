package collections_maps.obywatele;

import java.util.*;

public class RejestrObywateli {
    private Map<Long, Obywatel> obywatele = new HashMap<>();

    public void dodajObywatela(Long PESEL, String name, String surname) {
        Obywatel obywatel = new Obywatel(PESEL, name, surname);
        obywatele.put(PESEL, obywatel);
    }

    /**
     * @param rok - rok podany do 99 w formacie 4 cyfrowym, np. 1992
     * @return lista obywateli urodzonych przed podanym rokiem.
     */
    public List<Obywatel> znajdzUrodzonychPrzedRokiem(int rok) {
        List<Obywatel> list = new ArrayList<>();

        rok -= 1900;

        for (Obywatel obywatel : obywatele.values()) {
            int rocznik = obywatel.getRocznik();

            if (rocznik <= rok) {
                list.add(obywatel);
            }
        }

        return list;
    }

    public List<Obywatel> znajdzUrodzonychWRokuZImieniem(int rok, String imie) {
        List<Obywatel> list = new ArrayList<>();

        rok -= 1900;

        for (Obywatel obywatel : obywatele.values()) {
            int rocznik = obywatel.getRocznik();

            if (rocznik == rok && obywatel.getName().equals(imie)) {
                list.add(obywatel);
            }
        }

        return list;
    }

    public List<Obywatel> znajdzObywatelaPoNazwisku(String nazwisko) {
        List<Obywatel> list = new ArrayList<>();

        for (Obywatel obywatel : obywatele.values()) {
            if (obywatel.getSurname().equalsIgnoreCase(nazwisko)) {
                list.add(obywatel);
            }
        }

        return list;
    }

    public Optional<Obywatel> znajdzObywatelaPoPeselu(Long pesel) {
        for (Obywatel obywatel : obywatele.values()) {
            if (obywatel.getPESEL() == (pesel)) {
                return Optional.of(obywatel);
            }
        }
        return Optional.empty();
    }
}
