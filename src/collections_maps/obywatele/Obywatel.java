package collections_maps.obywatele;

public class Obywatel {
    private Long PESEL;
    private String name;
    private String surname;

    public Obywatel(Long PESEL, String name, String surname) {
        this.PESEL = PESEL;
        this.name = name;
        this.surname = surname;
    }

    public int getRocznik(){
        return Integer.parseInt(String.valueOf(getPESEL()).substring(0, 2));
    }

    public Long getPESEL() {
        return PESEL;
    }

    public void setPESEL(Long PESEL) {
        this.PESEL = PESEL;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public String toString() {
        return "Obywatel{" +
                "PESEL=" + PESEL +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                '}';
    }
}
