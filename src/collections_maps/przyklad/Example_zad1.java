package collections_maps.przyklad;

import java.util.Map;
import java.util.TreeMap;

public class Example_zad1 {
    public static void main(String[] args) {

        Student s1 = new Student(100200L, "A1", "B");
        Student s2 = new Student(100300, "A2", "B" );
        Student s3 = new Student(100400L, "A3", "B");
        Student s4 = new Student(100500L, "A4", "B");
        Student s5 = new Student(100600L, "A5", "B");

        University university = new University();
        Map<Long, Student> map = new TreeMap<>();

        //
        university.addStudent(100200L, "A1", "B");
        university.addStudent(100300, "A2", "B");
        university.addStudent(100500L, "A4", "B");
        university.addStudent(100400L, "A3", "B");
        university.addStudent(100600L, "A5", "B");
        // ----
        map.put(s1.getNrIndeksu(), s1);
        map.put(s2.getNrIndeksu(), s2);
        map.put(s3.getNrIndeksu(), s3);
        map.put(s4.getNrIndeksu(), s4);
        map.put(s5.getNrIndeksu(), s5);
        //

        //
        System.out.println(university.containsStudent(100200));
        // ----
        if (map.containsKey(100200L)) {
            Student student = map.get(100200L);
            System.out.println("Mamy studenta o indeksie 100200");
        } else {
            System.out.println("Nie mamy studenta o indeksie 100200");
        }
        //

        //
        System.out.println(university.getStudent(100400));
        // ----
        if (map.containsKey(100400L)) {
            System.out.println(map.get(100400L));
        }
        //


        //
        System.out.println(university.studentsCount());
        // ----
        System.out.println(map.size());
        //

        //
        university.printAllStudents();
        // ----
        for (Student student : map.values()) {
            System.out.println(student);
        }
        //


    }
}
