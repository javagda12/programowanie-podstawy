package collections_maps.przyklad;

import java.util.HashMap;
import java.util.Map;

public class University {
    private Map<Long, Student> dziennik = new HashMap<>();

    void addStudent(long index, String name, String surname) {
        Student studentDoDodania = new Student(index, name, surname);
        dziennik.put(index, studentDoDodania);

        String tekst = "abcde";
        String uciety = tekst.substring(2);
    }

    boolean containsStudent(long index){
        return dziennik.containsKey(index);
    }

    Student getStudent(long index){
        if(!dziennik.containsKey(index)){
            throw new NoSuchStudentException("Nie ma takiego studenta o indeksie: " + index);
        }
        return dziennik.get(index);
    }

    int studentsCount(){
        return dziennik.size();
    }

    void printAllStudents(){
        for (Student st: dziennik.values()) {
            System.out.println(st);
        }
    }
}
