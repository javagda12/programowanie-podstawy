package collections_maps.przyklad;

public class NoSuchStudentException extends RuntimeException {
    public NoSuchStudentException(String message) {
        super(message);
    }
}
