package collections_set_11.zad5;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Student> list = new ArrayList<>();
        list.add(new Student("123", "A", "B", Gender.MALE));
        list.add(new Student("124", "Marian", "C", Gender.FEMALE));
        list.add(new Student("126", "Marian", "D", Gender.MALE));
        list.add(new Student("129", "Marek", "D", Gender.MALE));

        System.out.println(list);
        list.forEach(System.out::println);

        // kobiety
        for (Student s : list) {
            if (s.getGender() == Gender.FEMALE) {
                System.out.println(s);
            }
        }

        //
        for (Student s : list) {
            if (s.getGender() == Gender.MALE) {
                System.out.println(s);
            }
        }

        // numery indeksów
        for (Student s : list) {
            System.out.println(s.getNrIndeksu());
        }
    }
}
