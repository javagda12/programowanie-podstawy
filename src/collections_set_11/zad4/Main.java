package collections_set_11.zad4;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        Random generator = new Random();

        for (int i = 0; i < 100; i++) {
            list.add(String.valueOf(generator.nextInt(100)));
        }

        double srednia = 0.0;
        for (String wartosc : list) {
            srednia += Double.parseDouble(wartosc);
        }
        srednia /= list.size();
        System.out.println("Srednia: " + srednia);

        /// źleeeeeeee
//        for (String wartosc : list) {
//            if (Double.parseDouble(wartosc) > srednia) {
//                list.remove(wartosc);
//            }
//        }

        // ok, iteracja od pierwszego elementu
//        for (int i = 0; i < list.size(); i++) {
//            double wartosc = Double.parseDouble(list.get(i));
//            if (wartosc > srednia) {
//                list.remove(i);
//                i--;
//            }
//        }

        // iteracja od ostatniego elementu
//        for (int i = list.size() - 1; i >= 0; i--) {
//            double wartosc = Double.parseDouble(list.get(i));
//            if (wartosc > srednia) {
//                list.remove(i);
//            }
//        }

        // posługując się iteratorem
        Iterator<String> it = list.iterator();
        while (it.hasNext()) {
            String value = it.next();
            double wartosc = Double.parseDouble(value);
            if (wartosc > srednia) {
                it.remove();
            }
        }

        System.out.println(list);

        String[] tablica = new String[list.size()];
//        for (int i = 0; i < list.size(); i++) {
//            tablica[i] = list.get(i);
//        }
        tablica = list.toArray(tablica);
        System.out.println(Arrays.toString(tablica));

    }
}
