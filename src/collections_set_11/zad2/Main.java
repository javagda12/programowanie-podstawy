package collections_set_11.zad2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        List<Integer> lista = new ArrayList<>();
        Random generator = new Random();

        // losowanie 10 liczb
        for (int i = 0; i < 10; i++) {
            lista.add(generator.nextInt(100));
        }

        double suma = 0;
        for (Integer element : lista) {
            suma += element;
        }

        double srednia = suma / lista.size();
        System.out.println(suma);
        System.out.println(lista);
        System.out.println(srednia);

        ArrayList<Integer> kopia = new ArrayList<>(lista);
        Collections.sort(kopia);

        System.out.println(kopia);
        double mediana = 0.0;
        if (kopia.size() % 2 == 0) {
            mediana = (kopia.get(kopia.size() / 2 - 1) + kopia.get(kopia.size() / 2)) / 2.0;
        } else {
            mediana = kopia.get(kopia.size() / 2);
        }
        System.out.println("Mediana: " + mediana);

        int max = kopia.get(kopia.size() - 1); // ostatni element posortowanej listy;
        int min = kopia.get(0); // pierwszy element posortowanej listy;

        System.out.println("Max: " + max);
        System.out.println("Min: " + min);
        System.out.println("Index min: " + lista.indexOf(min));
        System.out.println("Index max: " + lista.indexOf(max));
    }
}
