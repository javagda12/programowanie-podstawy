package collections_set_11.zad3;

import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.addAll(Arrays.asList("10030", "3004", "4000", "12355", "12222", "67888", "111200", "225355", "2222", "1111", "3546", "138751", "235912"));

        System.out.println("A, indeks 138751: " + list.indexOf("138751"));
        System.out.println("B, 67888: " + list.contains("67888"));
        System.out.println("C, 67889: " + list.contains("67889"));
        System.out.println("C, indexof 67889: " + list.indexOf("67889"));

        boolean udaloSie = list.remove("67888");
        System.out.println("Usuniecie zakonczone powodzeniem: " + udaloSie);

        System.out.println("A, indeks 138751: " + list.indexOf("138751"));
        System.out.println("B, 67888: " + list.contains("67888"));
        System.out.println("C, 67889: " + list.contains("67889"));
        System.out.println("C, indexof 67889: " + list.indexOf("67889"));

        System.out.println(list);
        for (String element : list) {
            System.out.println(element);
        }

    }
}
