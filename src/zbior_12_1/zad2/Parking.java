package zbior_12_1.zad2;

import java.util.ArrayList;
import java.util.List;

public class Parking {
    private List<MiejsceParkingowe> miejscaParkingowe = new ArrayList<>();
    private boolean czyOtwarty;
    private int licznikMiejscParkingowych;

    public Parking(boolean czyOtwarty) {
        this.czyOtwarty = czyOtwarty;
    }

    public void dodajMiejsceParkingowe() {
        miejscaParkingowe.add(new MiejsceParkingowe(licznikMiejscParkingowych++, StanMiejsca.WOLNE));
    }

    /**
     * Metoda sprawdza dostepnosc miejsca
     *
     * @param numerMiejsca
     * @return true jesli jest wolne, false jesli zajete
     */
    public boolean sprawdzDostepnoscMiejsca(int numerMiejsca) {
        for (MiejsceParkingowe miejsce : miejscaParkingowe) {
            if (miejsce.getNumerMiejsca() == numerMiejsca) {
                boolean czyWolne = miejsce.getStanMiejsca() == StanMiejsca.WOLNE;
                return czyWolne;
            }
        }

        // alternatywnie - rzucamy exception
        return false;
    }

    /**
     * Metoda zmiany stanu zajętości miejsca.
     * @param numerMiejsca
     * @return true jeśli udało się zająć miejsce, false jeśli się nie udało (zajęte lub nie istnieje).
     */
    public boolean zajmijMiejsce(int numerMiejsca) {
        for (MiejsceParkingowe miejsce : miejscaParkingowe) {
            if (miejsce.getNumerMiejsca() == numerMiejsca) {
                // znalazlem miejsce
                if(miejsce.getStanMiejsca() == StanMiejsca.WOLNE){
                    miejsce.setStanMiejsca(StanMiejsca.ZAJETE);
                    return true;
                }else {
                    // co jeśli miejsce jest zajete?
                    System.err.println("Miejsce jest już zajęte! Nie można zająć.");
                }
            }
        }
        return false;
    }

    public List<MiejsceParkingowe> getMiejscaParkingowe() {
        return miejscaParkingowe;
    }

    public void setMiejscaParkingowe(List<MiejsceParkingowe> miejscaParkingowe) {
        this.miejscaParkingowe = miejscaParkingowe;
    }

    public boolean isCzyOtwarty() {
        return czyOtwarty;
    }

    public void setCzyOtwarty(boolean czyOtwarty) {
        this.czyOtwarty = czyOtwarty;
    }
}
