package zbior_12_1.zad2;

public class MiejsceParkingowe {
    private static int licznikMiejsc = 0;

    private int numerMiejsca;
    private StanMiejsca stanMiejsca;

    public MiejsceParkingowe(StanMiejsca stanMiejsca) {
        this.numerMiejsca = licznikMiejsc++;
        this.stanMiejsca = stanMiejsca;
    }

    public MiejsceParkingowe(int numerMiejsca, StanMiejsca stanMiejsca) {
        this.numerMiejsca = numerMiejsca;
        this.stanMiejsca = stanMiejsca;
    }

    public int getNumerMiejsca() {
        return numerMiejsca;
    }

    public void setNumerMiejsca(int numerMiejsca) {
        this.numerMiejsca = numerMiejsca;
    }

    public StanMiejsca getStanMiejsca() {
        return stanMiejsca;
    }

    public void setStanMiejsca(StanMiejsca stanMiejsca) {
        this.stanMiejsca = stanMiejsca;
    }
}
