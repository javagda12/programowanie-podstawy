package zbior_12_1.zad3;

public class Main {
    public static void main(String[] args) {
        CentrumKonferencyjne centrumKonferencyjne = new CentrumKonferencyjne();
        centrumKonferencyjne.dodajSaleKonferencyjna(new SalaKonferencyjna(false, 20, 2000.0));
        centrumKonferencyjne.dodajSaleKonferencyjna(new SalaKonferencyjna(false, 21, 4000.0));
        centrumKonferencyjne.dodajSaleKonferencyjna(new SalaKonferencyjna(true, 10, 4000.0));

        // część 1^

        centrumKonferencyjne.wylistujSale();

        System.out.println(centrumKonferencyjne.znajdzNajtanszaSale(21));
    }
}
