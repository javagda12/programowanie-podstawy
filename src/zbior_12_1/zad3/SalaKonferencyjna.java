package zbior_12_1.zad3;

public class SalaKonferencyjna {
    private static int LICZNIK_SAL = 0;
    private int numerSali;
    private boolean czyZajeta;
    private int iloscMiejsc;
    private double cena;

    public SalaKonferencyjna(boolean czyZajeta, int iloscMiejsc, double cena) {
        this.numerSali = LICZNIK_SAL++;
        this.czyZajeta = czyZajeta;
        this.iloscMiejsc = iloscMiejsc;
        this.cena = cena;
    }

    public int getNumerSali() {
        return numerSali;
    }

    public void setNumerSali(int numerSali) {
        this.numerSali = numerSali;
    }

    public boolean isCzyZajeta() {
        return czyZajeta;
    }

    public void setCzyZajeta(boolean czyZajeta) {
        this.czyZajeta = czyZajeta;
    }

    public int getIloscMiejsc() {
        return iloscMiejsc;
    }

    public void setIloscMiejsc(int iloscMiejsc) {
        this.iloscMiejsc = iloscMiejsc;
    }

    public double getCena() {
        return cena;
    }

    public void setCena(double cena) {
        this.cena = cena;
    }

    @Override
    public String toString() {
        return "SalaKonferencyjna{" +
                "numerSali=" + numerSali +
                ", czyZajeta=" + czyZajeta +
                ", iloscMiejsc=" + iloscMiejsc +
                ", cena=" + cena +
                '}';
    }
}
