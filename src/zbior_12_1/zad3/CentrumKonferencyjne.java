package zbior_12_1.zad3;

import java.util.*;

public class CentrumKonferencyjne {
    private boolean czyOtwarte;
    private List<SalaKonferencyjna> listaSal = new ArrayList<>();

    public boolean dodajSaleKonferencyjna(SalaKonferencyjna sala) {
        for (SalaKonferencyjna salaKonferencyjna : listaSal) {
            if (salaKonferencyjna.getNumerSali() == sala.getNumerSali()) {
                // nie udało się dodać
                return false;
            }
        }
        listaSal.add(sala);
        return true;
    }

    public void wylistujSale() {
        for (SalaKonferencyjna sala : listaSal) {
            System.out.println(sala);
        }
    }

    private List<SalaKonferencyjna> znajdzWolne() {
        List<SalaKonferencyjna> listaWolnych = new ArrayList<>();
        for (SalaKonferencyjna sala : listaSal) {
            if (!sala.isCzyZajeta()) {
                listaWolnych.add(sala);
            }
        }
        return listaWolnych;
    }

    public void sortujPoCenie(List<SalaKonferencyjna> lista) {
        // sortujemy, więc sale będą ustawione po cenie (rosnąco)
        Collections.sort(lista, new Comparator<SalaKonferencyjna>() {
            @Override
            public int compare(SalaKonferencyjna o1, SalaKonferencyjna o2) {
                if (o1.getCena() > o2.getCena()) {
                    return 1;
                } else if (o2.getCena() > o1.getCena()) {
                    return -1;
                }
                return 0;
            }
        });
    }

    public Optional<SalaKonferencyjna> znajdzNajtanszaSale(int iloscPotrzebnychMiejsc) {
        List<SalaKonferencyjna> wolne = znajdzWolne();

        sortujPoCenie(wolne);

        for (SalaKonferencyjna sala : wolne) {
            if (sala.getIloscMiejsc() >= iloscPotrzebnychMiejsc) {
                return Optional.of(sala);
            }
        }
        return Optional.empty();
    }

    public Optional<SalaKonferencyjna> znajdzNajtanszaSale() {
        List<SalaKonferencyjna> wolne = znajdzWolne();

        sortujPoCenie(wolne);

        if (!wolne.isEmpty()) {
            return Optional.ofNullable(wolne.get(0));
        }
        return Optional.empty();
    }

    public boolean zarezerwujSale(int numerSali) {
        for (SalaKonferencyjna sala : listaSal) {
            if (sala.getNumerSali() == numerSali) {
                if (!sala.isCzyZajeta()) {
                    sala.setCzyZajeta(true);
                    return true;
                } else {
                    return false;
                }
            }
        }
        return false;
    }

    public void wylistujWolneSale() {
        List<SalaKonferencyjna> wolne = znajdzWolne();
        for (SalaKonferencyjna sala : wolne) {
            System.out.println(sala);
        }
    }

    public boolean zwolnijSale(int numerSali){
        for (SalaKonferencyjna sala : listaSal) {
            if (sala.getNumerSali() == numerSali) {
                if (!sala.isCzyZajeta()) {
                    sala.setCzyZajeta(false);
                    return true;
                } else {
                    return false;
                }
            }
        }
        return false;
    }

    public void wypiszStanSali(int numerSali){
        for (SalaKonferencyjna sala : listaSal) {
            if (sala.getNumerSali() == numerSali) {
                System.out.println(sala);
            }
        }
    }
}
