package zad7;

public class Main {
    public static void main(String[] args) {
        Person person = new Person("Marian", "Kowalski", 17);

        Club club = new Club();
        try {
            club.enter(person);
        } catch (NoAdultException e) {
            System.out.println(e.getMessage());
        }
    }
}
