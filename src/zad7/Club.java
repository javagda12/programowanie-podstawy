package zad7;

public class Club {
    public void enter(Person person) throws NoAdultException {
        if (person.getAge() < 18) {
            throw new NoAdultException("You are not adult");
        }
        System.out.println(person.getName() + " entering club.");
    }
}
