package zad7;

public class NoAdultException extends Exception{
    public NoAdultException(String message) {
        super(message);
    }
}
