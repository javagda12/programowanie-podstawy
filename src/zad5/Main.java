package zad5;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int liczba1 = scanner.nextInt();
        int liczba2 = scanner.nextInt();

        // a
        if (liczba2 == 0) {
            System.out.println("Odpowiedni komunikat - dzielisz przez 0");
        } else {
            System.out.println(liczba1 / liczba2);
        }

        // b
        try {
            System.out.println(liczba1 / liczba2);
        } catch (ArithmeticException ae) {
            System.err.println("Odpowiedni komunikat - dzielisz przez 0");
            System.err.println(ae.getMessage());
        }
    }
}
