package zad1;

import java.util.Random;

public class Losowanie {
    private Random generator;

    public Losowanie() {
        this.generator = new Random();
    }

    public int[] generujLiczbyCalkowite(int N) {
        int[] liczbyA = new int[N];
        for (int i = 0; i < N; i++) {
            // wygenerowanie i przypisanie do zmiennej
            int wartoscWygenerowana = generator.nextInt();

            // wpisanie do tablicy
            liczbyA[i] = wartoscWygenerowana;
            // wypisanie na ekran
            System.out.println(wartoscWygenerowana);
        }

        return liczbyA;
    }

    public double[] generujLiczbyZmiennoprzecinkowe(int N) {
        double[] liczbyB = new double[N];
        for (int i = 0; i < N; i++) {
            // wygenerowanie i przypisanie do zmiennej
            int wartoscWygenerowana = generator.nextInt();

            // wpisanie do tablicy
            liczbyB[i] = wartoscWygenerowana;
            // wypisanie na ekran
            System.out.println(wartoscWygenerowana);
        }

        return liczbyB;
    }

    public boolean[] generujBooleany(int N) {
        boolean[] liczbyC = new boolean[N];
        for (int i = 0; i < N; i++) {
            // wygenerowanie i przypisanie do zmiennej
            boolean wartoscWygenerowana = generator.nextBoolean();

            // wpisanie do tablicy
            liczbyC[i] = wartoscWygenerowana;
            // wypisanie na ekran
            System.out.println(wartoscWygenerowana);
        }

        return liczbyC;
    }

    public int[] generujLiczbyZZakresu(int N, int zakresGorny, int zakresDolny) {
        int szerokoscZakresu = zakresGorny - zakresDolny;
        int[] liczbyD = new int[N];
        for (int i = 0; i < N; i++) {
            // wygenerowanie i przypisanie do zmiennej
            int wartoscWygenerowana = generator.nextInt(szerokoscZakresu) + zakresDolny;

            // wpisanie do tablicy
            liczbyD[i] = wartoscWygenerowana;
            // wypisanie na ekran
            System.out.println(wartoscWygenerowana);
        }
        return liczbyD;
    }

    public double[] generujLiczbyZmiennoprzecinkoweZZakresu(int N, int zakresGorny, int zakresDolny) {
        int szerokoscZakresu = zakresGorny - zakresDolny;
        double[] liczbyE = new double[N];
        for (int i = 0; i < N; i++) {
            // wygenerowanie i przypisanie do zmiennej
            double wartoscWygenerowana = generator.nextDouble() * szerokoscZakresu + zakresDolny;

            // wpisanie do tablicy
            liczbyE[i] = wartoscWygenerowana;
            // wypisanie na ekran
            System.out.println(wartoscWygenerowana);
        }
        return liczbyE;
    }
}
