package zad1;

import java.util.Random;
import java.util.Scanner;

/*Napisz aplikację która pobiera od użytkownika N a nastepnie:
 losuje N liczb całkowitych (dowolny zakres) i wypisuje je na ekran
 losuje N liczb zmiennoprzecinkowych i wypisuje je na ekran
 losuje N razy wartość boolean i wypisuje je na ekran
 pobiera kolejne dwa parametry poczatekZakresu i koniecZakresu i losuje N liczb całkowitych z tego zakresu, a następnie wypisuje je na ekran
 pobiera kolejne dwa parametry poczatekZakresu i koniecZakresu i losuje N liczb zmiennoprzecinkowych z tego zakresu, a następnie wypisuje je na ekran*/
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random generator = new Random();

        System.out.println("Podaj N:");
        int N = scanner.nextInt();

        //a)
        int[] liczbyA = new int[N];
        for (int i = 0; i < N; i++) {
            // wygenerowanie i przypisanie do zmiennej
            int wartoscWygenerowana = generator.nextInt();

            // wpisanie do tablicy
            liczbyA[i] = wartoscWygenerowana;
            // wypisanie na ekran
            System.out.println(wartoscWygenerowana);
        }

        //b
        double[] liczbyB = new double[N];
        for (int i = 0; i < N; i++) {
            // wygenerowanie i przypisanie do zmiennej
            double wartoscWygenerowana = generator.nextDouble();

            // wpisanie do tablicy
            liczbyB[i] = wartoscWygenerowana;
            // wypisanie na ekran
            System.out.println(wartoscWygenerowana);
        }

        //c
        boolean[] liczbyC = new boolean[N];
        for (int i = 0; i < N; i++) {
            // wygenerowanie i przypisanie do zmiennej
            boolean wartoscWygenerowana = generator.nextBoolean();

            // wpisanie do tablicy
            liczbyC[i] = wartoscWygenerowana;
            // wypisanie na ekran
            System.out.println(wartoscWygenerowana);
        }


        System.out.println("Podaj zakres gorny:");
        int zakresGorny = scanner.nextInt();

        System.out.println("Podaj zakres dolny:");
        int zakresDolny = scanner.nextInt();

        // d
        int szerokoscZakresu = zakresGorny - zakresDolny;
        int[] liczbyD = new int[N];
        for (int i = 0; i < N; i++) {
            // wygenerowanie i przypisanie do zmiennej
            int wartoscWygenerowana = generator.nextInt(szerokoscZakresu) + zakresDolny;

            // wpisanie do tablicy
            liczbyD[i] = wartoscWygenerowana;
            // wypisanie na ekran
            System.out.println(wartoscWygenerowana);
        }

        //e

        double[] liczbyE = new double[N];
        for (int i = 0; i < N; i++) {
            // wygenerowanie i przypisanie do zmiennej
            double wartoscWygenerowana = generator.nextDouble() * szerokoscZakresu + zakresDolny;

            // wpisanie do tablicy
            liczbyE[i] = wartoscWygenerowana;
            // wypisanie na ekran
            System.out.println(wartoscWygenerowana);
        }
    }
}
