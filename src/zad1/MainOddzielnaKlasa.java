package zad1;

import java.util.Random;
import java.util.Scanner;

public class MainOddzielnaKlasa {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj N:");
        int N = scanner.nextInt();

        Losowanie losowanie = new Losowanie();
        losowanie.generujLiczbyCalkowite(N);


        System.out.println("Podaj zakres gorny:");
        int zakresGorny = scanner.nextInt();

        System.out.println("Podaj zakres dolny:");
        int zakresDolny = scanner.nextInt();

        int[] wynik = losowanie.generujLiczbyZZakresu(N, zakresGorny, zakresDolny);
    }
}
