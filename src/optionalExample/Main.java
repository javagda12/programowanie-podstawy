package optionalExample;

import java.util.Optional;

public class Main {
    public static void main(String[] args) {
        Ksiegarnia ksiegarnia = new Ksiegarnia(new Ksiazka[]{new Ksiazka("ja", "Wladcy much", 1999)});

        Optional<Ksiazka> k = ksiegarnia.znajdzKsiazkeOTytule("Wladcy much");
        if(k.isPresent()) {
            Ksiazka p = k.get();

            System.out.println(p.getRokWydania());
        }
        // k.orElse(null/lub inna ksiazka);
    }
}
