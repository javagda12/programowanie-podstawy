package optionalExample;

public class Ksiazka {
    private String autor, tytul;
    private int rokWydania;

    public Ksiazka(String autor, String tytul, int rokWydania) {
        this.autor = autor;
        this.tytul = tytul;
        this.rokWydania = rokWydania;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getTytul() {
        return tytul;
    }

    public void setTytul(String tytul) {
        this.tytul = tytul;
    }

    public int getRokWydania() {
        return rokWydania;
    }

    public void setRokWydania(int rokWydania) {
        this.rokWydania = rokWydania;
    }
}
