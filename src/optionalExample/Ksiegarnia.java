package optionalExample;

import java.util.Optional;

public class Ksiegarnia {
    private Ksiazka[] ksiazkiWKsiegarni;

    public Ksiegarnia(Ksiazka[] ksiazkiWKsiegarni) {
        this.ksiazkiWKsiegarni = ksiazkiWKsiegarni;
    }

    public Optional<Ksiazka> znajdzKsiazkeOTytule(String tytul) {

//        for (int i = 0; i < ksiazkiWKsiegarni.length; i++) {
        for (Ksiazka k : ksiazkiWKsiegarni) {
            if (k.getTytul().equals(tytul)) {

                return Optional.of(k);
            }
        }
        return Optional.empty();
    }
}

