package genericsExample;

import example.Matka;
import example.Ojciec;
import example.Osoba;

public class Main {
    public static void main(String[] args) {
        Ojciec o1 = new Ojciec("Marian");
//        Matka o1 = new Matka("Marian");
        Ojciec o2 = new Ojciec("Gienek");
        Para<Ojciec> dwieOsoby = new Para<>(o1, o2);

        Object oX = dwieOsoby.getLewy();
        if (oX instanceof Ojciec) {
            Ojciec oXCasted = (Ojciec) oX;
            oXCasted.przedstawSie();
        }
    }
}
