package collections_sets;

import collections_set_11.zad6.Student;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
//        Set<Integer> studenty = new HashSet<>();
//        HashSet<Integer> inty = new HashSet<>(Arrays.asList(1,2,3,4,5));
        HashSet<Student> studenty = new HashSet<>();

        Student student1 = new Student("75", "c", "z");
        Student student2 = new Student("76", "c", "z");

        studenty.add(student1);
        studenty.add(student2);
        System.out.println(studenty);
//        for (int i = 0; i < studenty.size(); i++) {
//            studenty.
//        }

        // pobieramy iterator
        Iterator<Student> iterator = studenty.iterator();
        // dopóki iterator posiada następny element
        while (iterator.hasNext()){
            // przechodzimy do niego - wywołanie next przechodzi do następnego elementu
            // oraz zwraca ten element
            // UWAGA! nie wykonywać metody 'next' więcej niż raz na obieg pętli
            Student student = iterator.next();

            /// ~~~ czynnosci na obiekcie

            // warunek usunięcia
            if (student.getIndex().equals("23")){
                // usuwanie iteratorem
                iterator.remove();
            }
        }

        Student[] students = new Student[studenty.size()];
        students = studenty.toArray(students);
        for (Student s : students) {
            if (s.getIndex().equals("23")){
                studenty.remove(s);
            }
        }
    }
}
