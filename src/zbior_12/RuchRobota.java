package zbior_12;

public enum RuchRobota {
    KROK_LEWA(5), KROK_PRAWA(5), RUCH_REKA_LEWA(5), RUCH_REKA_PRAWA(5), SKOK(10);

    private int kosztBaterii;

    RuchRobota(int kosztBaterii) {
        this.kosztBaterii = kosztBaterii;
    }

    public int getKosztBaterii() {
        return kosztBaterii;
    }
}
