package zbior_12;

import java.text.ParseException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Robot robot = new Robot(100, "Maniek", true);

        robot.poruszRobotem(RuchRobota.RUCH_REKA_LEWA);
        robot.poruszRobotem(RuchRobota.RUCH_REKA_PRAWA);
        robot.poruszRobotem(RuchRobota.SKOK);
        robot.poruszRobotem(RuchRobota.SKOK);
        robot.poruszRobotem(RuchRobota.SKOK);
        robot.poruszRobotem(RuchRobota.SKOK);
        robot.poruszRobotem(RuchRobota.SKOK);
        robot.poruszRobotem(RuchRobota.SKOK);
        robot.poruszRobotem(RuchRobota.KROK_LEWA);
        robot.poruszRobotem(RuchRobota.KROK_PRAWA);
        robot.poruszRobotem(RuchRobota.SKOK);
        robot.poruszRobotem(RuchRobota.SKOK);
        robot.poruszRobotem(RuchRobota.SKOK);

        Scanner scanner = new Scanner(System.in);

        String linia = scanner.nextLine();
        try {
            RuchRobota ruch = RuchRobota.valueOf(linia.toUpperCase().trim());
        }catch (IllegalArgumentException pe){
            System.err.println(pe.getMessage());
        }
    }
}
