package zbior_12;

public class Robot {
    private int poziomBaterii;
    private String nazwa;
    private boolean czyWlaczony;

    public Robot(int poziomBaterii, String nazwa, boolean czyWlaczony) {
        this.poziomBaterii = poziomBaterii;
        this.nazwa = nazwa;
        this.czyWlaczony = czyWlaczony;
    }

    public void poruszRobotem(RuchRobota ruch) {
        // żeby nie tworzyć kodu kilkakrotnie zagnieżdzonego sprawdzam warunek
        // wcześnie w metodzie i jeśli nie jest spełniony wykonuję return
        if (!czyWlaczony) {
            System.err.println("Robot nie jest włączony");
            return;
        }

        // pozostała logika metody
        if (poziomBaterii >= ruch.getKosztBaterii()) {
            poziomBaterii -= ruch.getKosztBaterii();
            System.out.println("Wykonano ruch");
        } else {
            System.err.println("Brak mocy.");
        }
    }

    // alternatywa -
//    public void poruszRobotem(RuchRobota ruch) {
//        if (czyWlaczony) {
//            if (poziomBaterii >= ruch.getKosztBaterii()) {
//                poziomBaterii -= ruch.getKosztBaterii();
//                System.out.println("Wykonano ruch");
//            } else {
//                System.err.println("Brak mocy.");
//            }
//        }
//    }

    public void naladujRobota() {
        poziomBaterii = 100;
    }

    public void wlaczRobota() {
        czyWlaczony = true;
    }

    public void wylaczRobota() {
        czyWlaczony = false;
    }

    public int getPoziomBaterii() {
        return poziomBaterii;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public boolean isCzyWlaczony() {
        return czyWlaczony;
    }
}
