package example;

public class Ojciec extends  Osoba implements ICzlonekRodziny {

    public Ojciec(String name) {
        super(name);
    }

    @Override
    public void przedstawSie() {
        System.out.println("i am father, my name is: " + name);
    }

    @Override
    public boolean czyDorosly() {
        return true;
    }

    @Override
    public String toString() {
        return "Ojciec{" +
                "name='" + name + '\'' +
                '}';
    }
}
