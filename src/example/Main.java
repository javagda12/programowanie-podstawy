package example;

public class Main {

    public static void main(String[] args) {
        ICzlonekRodziny[] rodzina = new ICzlonekRodziny[4];
        rodzina[0] = new Ojciec("Marian");
        rodzina[1] = new Matka("Gosia");
        rodzina[2] = new Syn("Janek");
        rodzina[3] = new Corka("Ala");

        for (ICzlonekRodziny czlonek : rodzina) {

            // jeśli
            if(czlonek instanceof Ojciec){
                System.out.println("Przyniescie mi piwko!");
            }else{
                czlonek.przedstawSie();
            }


            System.out.println("Jestem dorosły: " + czlonek.czyDorosly());
        }

    }
}
