package example;

public class Matka extends Osoba implements ICzlonekRodziny {

    public Matka(String name) {
        super(name);
    }

    @Override
    public void przedstawSie() {
        System.out.println("i am mother - my name is: " + name);
    }

    @Override
    public boolean czyDorosly() {
        return true;
    }
}
