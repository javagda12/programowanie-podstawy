package example;

public class Corka extends Osoba implements ICzlonekRodziny {

    public Corka(String name) {
        super(name);
    }

    @Override
    public void przedstawSie() {
        System.out.println("i am daughter");
    }

    @Override
    public boolean czyDorosly() {
        return false;
    }
}
