package example;

public class Ciocia extends Osoba implements ICzlonekRodziny {
    public Ciocia(String name) {
        super(name);
    }

    @Override
    public boolean czyDorosly() {
        return true;
    }
}
