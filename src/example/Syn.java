package example;

public class Syn extends Osoba implements ICzlonekRodziny {

    public Syn(String name) {
        super(name);
    }

    @Override
    public void przedstawSie() {
        System.out.println("who's asking?");
    }

    @Override
    public boolean czyDorosly() {
        return false;
    }
}
