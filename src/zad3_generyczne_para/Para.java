package zad3_generyczne_para;

public class Para<T> {
    private T lewy;
    private T prawy;

    public Para(T lewy, T prawy) {
        this.lewy = lewy;
        this.prawy = prawy;
    }

    public T getLewy() {
        return lewy;
    }

    public void setLewy(T lewy) {
        this.lewy = lewy;
    }

    public T getPrawy() {
        return prawy;
    }

    public void setPrawy(T prawy) {
        this.prawy = prawy;
    }

    public boolean jestNiepuste() {
        return lewy != null && prawy != null;
    }
}
