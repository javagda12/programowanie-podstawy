package zad3_generyczne_para;

public class Main {
    public static void main(String[] args) {

    }

    public static Para[] znajdzNiepuste(Para[] pary) {
        // jeden obieg dla sprawdzenia ile jest elementów niepustych
        int ile = 0;
        for (int i = 0; i < pary.length; i++) {
            if (pary[i].jestNiepuste()) {
                ile++;
            }
        }
        // drugi do przepisania niepustych do nowej tablicy
        int indeksPodtablicy = 0;
        Para[] podtablica = new Para[ile];
        for (int i = 0; i < pary.length; i++) {
            if (pary[i].jestNiepuste()) {
                podtablica[indeksPodtablicy++] = pary[i];
            }
        }
        return podtablica;
    }
}
