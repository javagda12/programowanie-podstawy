package collectionsExample;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class ComparatorMain {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();

        list.addAll(Arrays.asList(1, 7, 4, 8, 4, 6, 2, 9, 1));

        List<Integer> kopia = new ArrayList<>(list);


        Comparator<Integer> komparator = new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                if(o1 > o2){
                    return 1;
                }else if(o2 > o1){
                    return -1;
                }
                return 0;
            }
        };
        list.sort(komparator);                  // opcja A
        list.sort(new KomparatorIntegerow());   // opcja B

        list.sort(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                if(o1 > o2){
                    return 1;
                }else if(o2 > o1){
                    return -1;
                }
                return 0;
            }
        });   // opcja C

    }
}
