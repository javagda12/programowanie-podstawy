package collectionsExample;

import example.Ojciec;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        int[] tablica = new int[200];

        List<Ojciec> lista = new ArrayList<>();

        System.out.println("Size: " + lista.size());
        System.out.println("Length: " + tablica.length);

//        lista.add(3);
//        lista.add(2);
//        lista.add(4);
//        lista.add(5);
        lista.add(new Ojciec("m"));
        lista.add(new Ojciec("d"));
        lista.add(new Ojciec("c"));

        tablica[0] = 55;
        tablica[1] = 5;
        tablica[2] = 56;
        tablica[3] = 57;
        System.out.println(tablica);


//        lista.addAll(Arrays.asList(3, 5, 6, 7, 8, 9));
        System.out.println(lista);

        lista.remove(((Object)5));

//        for (Integer element : lista) {
//            System.out.println(element);
//        }

        for (int i = 0; i < lista.size(); i++) {
            System.out.println(lista.get(i));
        }

//        for (int i = 0; i < tablica.length; i++) {
//            System.out.println(tablica[i]);
//        }

        if(lista.size() == 0){

        }

        if (lista.isEmpty()){

        }
    }
}
