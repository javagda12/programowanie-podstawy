package zad6;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int A = scanner.nextInt();
        int B = scanner.nextInt();
        int C = scanner.nextInt();

        QuadraticEquation equation = new QuadraticEquation();
        try {
            equation.calculate(A, B, C);
        }catch (DetlaEqualsZeroException deze){
            System.out.println(deze.getMessage());
        }
    }
}
