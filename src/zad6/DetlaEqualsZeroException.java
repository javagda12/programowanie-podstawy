package zad6;

public class DetlaEqualsZeroException extends RuntimeException {
    public DetlaEqualsZeroException(String message) {
        super(message);
    }
}
