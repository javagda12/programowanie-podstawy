package zad6;

public class QuadraticEquation {
    public void calculate(int A, int B, int C) {
        double delta = (B * B) - (4 * A * C);

        if (delta == 0) {
            double x0 = (B / (2 * A));
            System.out.println(x0);
        } else if (delta > 0) {
            double x1 = (-B + (Math.sqrt(delta)) / (2 * A));
            double x2 = (-B - (Math.sqrt(delta)) / (2 * A));
            System.out.println(x1 + " - " + x2);
        } else {
            throw new DetlaEqualsZeroException("Delta is equal zero");
        }
        System.out.println("Coś wypisze");
    }
}
