package zad4;

public class Laptop extends Komputer {
    private double wielkoscMatrycy;
    private boolean czyPosiadaRetine;

    public Laptop(int moc, String producent, TypProcesora typProcesora, double wielkoscMatrycy, boolean czyPosiadaRetine) {
        super(moc, producent, typProcesora);
        this.wielkoscMatrycy = wielkoscMatrycy;
        this.czyPosiadaRetine = czyPosiadaRetine;
    }
}
