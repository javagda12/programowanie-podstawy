package zad4;

public class Komputer {
    private int moc;
    private String producent;
    private TypProcesora typProcesora;

    public Komputer(int moc, String producent, TypProcesora typProcesora) {
        this.moc = moc;
        this.producent = producent;
        this.typProcesora = typProcesora;
    }
}
