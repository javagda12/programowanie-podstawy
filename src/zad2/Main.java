package zad2;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj dochod:");

        double podatek = 0.0;

        double dochod = scanner.nextDouble();
        if (dochod <= 85528) {
            podatek = dochod * 0.18 - 556.02;
        } else {
            podatek = (dochod - 85528) * .32 + 14839.02;
        }

        System.out.println(String.format("Podatek: %.2f ", podatek));

        // rozwiązanie drugie
        OsobaFizyczna osobaFizyczna = new OsobaFizyczna(dochod);
        System.out.println(osobaFizyczna.obliczPodatek());
    }
}
