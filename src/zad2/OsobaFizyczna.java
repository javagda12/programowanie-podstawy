package zad2;

public class OsobaFizyczna {
    private double dochod;

    public OsobaFizyczna(double dochod) {
        this.dochod = dochod;
    }

    public double obliczPodatek() {
        double podatek = 0.0;
        if (dochod <= 85528) {
            podatek = dochod * 0.18 - 556.02;
        } else {
            podatek = (dochod - 85528) * .32 + 14839.02;
        }

        return podatek;
    }
}
